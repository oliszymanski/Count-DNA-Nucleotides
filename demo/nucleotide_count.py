#================================================
#   CLASSES
#================================================

class CountNucleotides:

    def __init__(self):     # for initializing class

        return




    def convert_elements_to_string(self, ls):

        ls = [str(element) for element in ls]

        return ls




    def count_percentage(self, ls_amounts):

        ls_percentages = []

        all_data = sum(ls_amounts)


        for element_num in ls_amounts:
            percentage = ( element_num / all_data )

            percentage = int( ( percentage * 100 ) )       # rounding up the data ( to 1/100 place )
            ls_percentages.append(percentage)


        return ls_percentages




    def count_amount(self, s_file_path):

        ls_a_amount = []       # lists for inserting data
        ls_c_amount = []
        ls_g_amount = []
        ls_t_amount = []

        ls_data_amount = []



        ls_file = open(s_file_path, 'r')       # read the file in binary

        ls_file = self.convert_elements_to_string(ls_file)


        for line in ls_file:
            for element in line:

                if (element == 'A'): ls_a_amount.append(element)       # find adenine

                elif (element == 'C'): ls_c_amount.append(element)     # find cytosine

                elif (element == 'G'): ls_g_amount.append(element)     # find guanine

                elif (element == 'T'): ls_t_amount.append(element)     # find thymine


        ls_data_amount.append(  len(ls_a_amount)  )
        ls_data_amount.append(  len(ls_c_amount)  )
        ls_data_amount.append(  len(ls_g_amount)  )
        ls_data_amount.append(  len(ls_t_amount)  )

        ls_percentage = self.count_percentage(ls_data_amount)


        return ls_data_amount, ls_percentage




    def find_sequence( self, ls_meta, sequence ):

        """
        input:
            ls_meta:    metadata which theoretically contains the sequence,
            sequence:   sequence which is probably in the ls_data,

        output:
            number of given sequences in the genome;
        """

        ls_boolean = []

        for val in range(  0, len( ls_meta )  ):
            if (  ls_meta[ val : len( sequence ) + val ] == sequence  ):        # checking if metadata contains the sequence (going from i-th element to )
                ls_boolean.append(True)


        return len( ls_boolean )        # returning number of elements in a list




    def find_longest_sequence( self, ls_converted_data : list, ls_sequences : list  ):
        """
        input:
            ls_converted_data:      just converted data,
            ls_sequences:           list of sequences that will be checked;

        output:
            longest_seq:        longest sequence in a given genome;
        """

        seq_dict = {}       # adding sequence

        for sequence in ls_sequences:
            seq_len = self.find_sequence( ls_converted_data , sequence )        # finding the longest sequence
            seq_dict[ seq_len ] = sequence


        print("seq_dict =", seq_dict, '\n')


        return seq_len



#================================================
#   END OF FILE
#================================================