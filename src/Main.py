#================================================
#   IMPORTS
#================================================

import count_nucleotide as cn



#================================================
#   GLOBALS
#================================================

s_file_path = "src-data/Acetobacter_pasterianus_dna.txt"        # path tp bacteria genomes
s_second_file_path = "src-data/Escherichia_phage.txt"           # path to virus genomes
s_third_file_path = "src-data/sars-cov-2.txt"

ls_test_sequences = [ 'AC', 'GT', 'TG', 'CA', 'ACGT' ]



#================================================
#   DEBUGGERS
#================================================

_DBG0_ = False      # checking
_DBG8_ = False      # errors, warnings
_DBG9_ = False      # other errors



#================================================
#   MAIN
#================================================

if (__name__ == '__main__'):

    # testing the class
    search_nucleotides = cn.CountDnaNucleotides()          # main object



    nucleotide_number = search_nucleotides.count_nucleotides(s_file_path)       # acetobacter genome
    print("nucleotide_number =", nucleotide_number, "\n\n")


    nucleotide_percentage = search_nucleotides.count_nucleotide_percentage(s_file_path)
    print("nucleotide_percentage =", nucleotide_percentage, "\n\n\n\n")





    second_nucleotides_number = search_nucleotides.count_nucleotides(s_second_file_path)    # Escherichia genome
    print("second_nucleotides_number =", second_nucleotides_number, "\n\n")


    second_nucleotide_percentage = search_nucleotides.count_nucleotide_percentage(s_second_file_path)
    print("second_nucleotides_number =", second_nucleotide_percentage, "\n\n\n\n")





    third_nucleotide_number = search_nucleotides.count_nucleotides(s_third_file_path)
    print("third_nucleotide_number =", third_nucleotide_number, "")


    third_nucleotide_percentage = search_nucleotides.count_nucleotide_percentage(s_third_file_path)
    print("third_nucleotide_percentage =", third_nucleotide_percentage, '\n\n\n\n')


    # testing the display chart
    dict_nucleotide_number = search_nucleotides.get_nucleo_dict(s_third_file_path)
    print("dict_nucleotide_number =", dict_nucleotide_number)

    search_nucleotides.display_nucleotide_chart(dict_nucleotide_number)



    # find longest sequence testing
    ls_sequences = ['AC', 'GA', 'TC']

    dict_seq = {}

    st_0 = 'ACGGGTACTGATCACCCTGGATAAAAAAACACACGATCCC'

    ls_data_file = open( s_file_path, 'r' )     # opening and reading file with data
    ls_converted_data = search_nucleotides.convert_to_string( ls_data_file )


    d = search_nucleotides.find_longest_sequence( ls_converted_data, ls_sequences  )

    print("d =", d)



#================================================
#   END OF FILE
#================================================