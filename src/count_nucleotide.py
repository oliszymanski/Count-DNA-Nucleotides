#================================================
#   IMPORTS
#================================================

from pandas import DataFrame
import matplotlib.pyplot as plt


#================================================
#   CLASSES
#================================================

class CountDnaNucleotides:

    def __init__(self):
        '''Initializing class'''

        return

    # UPDATE NOTE:  make this capable of reading FAST and FASTQ files with this kind of data



    def convert_to_string(self, ls_data):
        """
            input:
                ls_data:    list with data,

            output:
                ls_data:    list with elements converted to string;


            description:
                converting all data into string values in a list going through
                every element in a given data set.
        """


        ls_data = [ str( element ) for element in ls_data ]

        return ls_data




    def get_nucleo_dict( self, file_path : str ):
        """
            input:
                file_path:              path of the file with the genome,

            output:
                 dict_nucleotide:       dictionary with nucleotide data;


            description:
                getting and gaining all the data from a given data file. All steps:

                    1. Going through and changing the data type of every element
                    2. adding them to a list which is made to store specific element
                    3. adding the number (length of every list) to the main list
                    4. adding that list (as a value) to the key called "nucleotides"
        """


        ls_s_a = []         # lists for nucleotides
        ls_s_c = []
        ls_s_g = []
        ls_s_t = []

        ls_all_data = []          # list for all data

        dict_nucleotide = {}      # main dictionary for nucleotide number list


        ls_data_file = open( file_path, 'r' )                   # opening file with data and reading it
        ls_fixed_data = self.convert_to_string(ls_data_file)    # every element (element is in every line) converted to string

        for line in ls_fixed_data:
            for element in line:

                if (element == 'A' or element == 'a'): ls_s_a.append(element)       # find adenine

                elif (element == 'C' or element == 'c'): ls_s_c.append(element)     # find cytosine

                elif (element == 'G' or element == 'g'): ls_s_g.append(element)     # find guanine

                elif (element == 'T' or element == 't'): ls_s_t.append(element)     # find thymine



        ls_all_data.append(  len( ls_s_a )  )
        ls_all_data.append(  len( ls_s_c )  )
        ls_all_data.append(  len( ls_s_g )  )
        ls_all_data.append( len( ls_s_t )  )


        dict_nucleotide["nucleotides"] = ls_all_data        # ls_all_data is the value of "nucleotides" key

        return dict_nucleotide




    def count_nucleotides(self, file_path : str):

        """
            input:
                file_path:      path of the file,

            output:
                dict_data:      dictionary of data about nucleotides;


            description:
                Counts all nucleotides by changing the data type of each element in a data
                set. After that, it checks the value of each nucleotide and it adds to a
                specified.

                After that, keys are created ("A", "C", "G" and "T") inside the empty dictionary
                and the function gets the number of nucleotides (from specified lists) and adds
                it to every specified keys ("A's" go to "A" key, "C's" go to "C" key etc. ).
        """


        ls_s_a = []         # lists for nucleotides
        ls_s_c = []
        ls_s_g = []
        ls_s_t = []

        dict_data = {}      # dictionary for data


        ls_data_file = open( file_path, 'r' )
        ls_fixed_data = self.convert_to_string(ls_data_file)


        for line in ls_fixed_data:
            for element in line:

                if (element == 'A' or element == 'a'): ls_s_a.append(element)       # find adenine

                elif (element == 'C' or element == 'c'): ls_s_c.append(element)     # find cytosine

                elif (element == 'G' or element == 'g'): ls_s_g.append(element)     # find guanine

                elif (element == 'T' or element == 't'): ls_s_t.append(element)     # find thymine



        dict_data["A"] = len(ls_s_a)
        dict_data["C"] = len(ls_s_c)
        dict_data["G"] = len(ls_s_g)
        dict_data["T"] = len(ls_s_t)


        return dict_data




    def count_nucleotide_percentage(self, file_path : str):
        '''
            input:
                file_path:      path of the file with data,

            output:
                dict_percent_data:  dictionary with percentage data


            description:
                Getting the nucleotides. Turning each element into a string and adding them
                to lists. Getting the number of each element and counting their percentage out of
                the number of all nucleotides.
        '''


        ls_s_a = []         # lists for nucleotides
        ls_s_c = []
        ls_s_g = []
        ls_s_t = []

        ls_all_data = []
        ls_nucleotide_percentage = []

        dict_nucleotide_percentage = {}      # dictionary for nucleotide percentage data


        ls_data_file = open( file_path, 'r' )
        ls_fixed_data = self.convert_to_string(ls_data_file)

        for line in ls_fixed_data:
            for element in line:

                if (element == 'A' or element == 'a'): ls_s_a.append(element)       # find adenine

                elif (element == 'C' or element == 'c'): ls_s_c.append(element)     # find cytosine

                elif (element == 'G' or element == 'g'): ls_s_g.append(element)     # find guanine

                elif (element == 'T' or element == 't'): ls_s_t.append(element)     # find thymine



        ls_all_data.append(len(ls_s_a))
        ls_all_data.append(len(ls_s_c))
        ls_all_data.append(len(ls_s_g))
        ls_all_data.append(len(ls_s_t))

        all_data = sum(ls_all_data)         # number of all nucleotides



        for value in ls_all_data:
            percent_value = round( (  value / all_data ) * 100, 3  )      # counting percentage of every nucleotide and rounding it to 1/1000 of a number
            ls_nucleotide_percentage.append( percent_value )



        dict_nucleotide_percentage["A"] = ls_nucleotide_percentage[0]
        dict_nucleotide_percentage["C"] = ls_nucleotide_percentage[1]
        dict_nucleotide_percentage["G"] = ls_nucleotide_percentage[2]
        dict_nucleotide_percentage["T"] = ls_nucleotide_percentage[3]


        return dict_nucleotide_percentage




    def display_nucleotide_chart(self, dict_data : dict):
        """
           input:
                dict_data:      dictionary of data,

           output:


            description:
                Getting data from the dict_data data type. By using pandas DataFrame
                (df in this function) it sets dict_data dictionary (which is the dictionary
                with percentage of every nucleotide) as main data. Using this data, to
                display a pie chart with the amount of every nucleotide.

        """


        ls_nucleotides = ['A', 'C', 'G', 'T']



        df = DataFrame(dict_data, index=ls_nucleotides)


        df.plot.pie( y='nucleotides', figsize=(5, 5), startangle=90 )
        plt.show()

        return




    def find_sequence(self, ls_meta, seq):
        """
            input:
                ls_meta:    metadata which theoretically contains the sequence,
                seq:        sequence which is probably in the ls_data;

            output:
                num_seq:    number of sequences in a given metadata;


            description:
                Enter data source (ls_meta in this function) and the searched sequence which
                is probably contained inside the data source. It goes from the first element
                in the data and from that element on to the n-th place (length of searched
                sequence). If the next elements are equal to sequence, then it adds
                a bool value of True;
        """


        ls_boolean = []

        for val in range(  0, len( ls_meta )  ):

            if (  ls_meta[ val : len(seq) + val ] == seq  ):        # checking if metadata contains the sequence (going from i-th element to )
                ls_boolean.append(True)


        return len( ls_boolean )




    def find_longest_sequence( self, ls_converted_data : list, ls_sequences : list  ):
        """
        input:
            ls_converted_data:      just converted data,
            ls_sequences:           list of sequences that will be checked;

        output:
            longest_seq:        longest sequence in a given genome;

        """

        # search sequences algorythm
        seq_dict = {}                               # adding sequence dictionary

        for searched_seq in ls_sequences:           # for every sequence in given sequences (ls_sequences) this operation will be executed
            seq_found = 0                           # number of sequences found

            for sequence in ls_converted_data:      # going through every line in a sequence

                nucleo_num = self.find_sequence( sequence, searched_seq )
                seq_found = seq_found + nucleo_num          # adding number of nucleotides from a given sequence


            seq_dict[ searched_seq ] = seq_found



        # searching greatest value inside a dictionary


        # compare all keys value algorythm

        found_greatest_val = 0                  # 1. add a comparing variable

        for searched_seq in ls_sequences:       # 2. looping through every keys' value (we can use list of sequences)


            # 3. check if the next key's value (in seq_dict) is greater than the next one
            if (  found_greatest_val <  seq_dict[ searched_seq ]  ):
                found_greatest_val = seq_dict[ searched_seq ]



        return found_greatest_val       # 4. return the greatest number



#================================================
#   END OF FILE
#================================================