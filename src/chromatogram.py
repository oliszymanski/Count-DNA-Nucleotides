#===============================================
#   IMPORTS
#===============================================

import matplotlib.pyplot as plt




#===============================================
#   CLASSES
#===============================================

class GraphData:

    def __init__(self):
         """
         initializing GraphData class

            taken parameters:
                no parameters
         """

         ls_graph = []  # main list for all data

         self.ls_graph = ls_graph



         ls_a = []      # for individual nucleotides
         ls_c = []
         ls_g = []
         ls_t = []

         self.ls_a = ls_a
         self.ls_c = ls_c
         self.ls_g = ls_g
         self.ls_t = ls_t


         return




    def display_graph(self, converted_data, display_grid : bool, max_range : int):
        """
            input:
                converted_data:     data that is converted to string

            output:

        """

        for sequence in converted_data:
            for data_element in sequence:
                if ( data_element == 'a' or data_element == 'A' ):      # for adenine
                    if (max_range == len(self.ls_a)): break

                    self.ls_a.append(1)

                    self.ls_c.append( 0 )
                    self.ls_g.append( 0 )
                    self.ls_t.append( 0 )





                elif ( data_element == 'c' or data_element == 'C' ):    # cytosine
                    if (max_range == len(self.ls_c)): break

                    self.ls_a.append( 0 )

                    self.ls_c.append( 2 )

                    self.ls_g.append( 0 )
                    self.ls_t.append( 0 )






                elif ( data_element == 'g' or data_element == 'G' ):    # guanine
                    if (max_range == len(self.ls_g)): break

                    self.ls_a.append( 0 )
                    self.ls_c.append( 0 )

                    self.ls_g.append( 3 )

                    self.ls_t.append( 0 )






                elif ( data_element == 't' or data_element == 'T' ):    # thymine
                    if (max_range == len(self.ls_t)): break

                    self.ls_a.append( 0 )
                    self.ls_c.append( 0 )
                    self.ls_g.append( 0 )

                    self.ls_t.append( 4 )





        plt.plot( self.ls_a, label="adenine" )
        plt.plot( self.ls_c, label="cytosine")
        plt.plot( self.ls_g, label="guanine" )
        plt.plot( self.ls_t, label="thymine" )

        plt.legend()
        plt.grid( display_grid )
        plt.show()

        return




    def convert_data(self, data_file_path : str, read_type : str):
        """
            input:
                data_file:  file with data that is going to be converted,

            output:
                converted_data: converted data;

        """

        ls_data = open( data_file_path, read_type )
        converted_data = [ str( data_element ) for data_element in ls_data ]       # converting to a string

        return converted_data




    def check_data(self, data_file_path : str, read_type : str):
        """
            input:
                file_path:  path leading to file with data

            description:
                this method will display all the data that is already
                in this file
        """


        opened_file = open(data_file_path, read_type)

        for val in opened_file:
            print(val)




#===============================================
#   END OF FILE
#===============================================